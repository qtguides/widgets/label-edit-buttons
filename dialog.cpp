#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QString texto = ui->lineEdit->text();
    QMessageBox::information(this, "Titulo de Ventana", texto);
    ui->lineEdit->setText("Te lo cambie! Ja!");
}
